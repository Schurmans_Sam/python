def bestaat_uit_bytes(s):
    elements = s.split(",")
    result = True
    for element in elements:
        num = int(element)
        if not (num >= 0 and num <= 255):
            result = False
    return result